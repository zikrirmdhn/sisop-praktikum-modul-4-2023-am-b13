# sisop-praktikum-modul-4-2023-AM-B13

## Praktikum modul 4

### Kelompok B-13 dengan anggota:
| **No** | **Nama** | **NRP** | 
| ------------- | ------------- | --------- |
| 1 | Gabriella Natasya Br Ginting  | 5025211081 | 
| 2 | Muhammad Zikri Ramadhan | 5025211085 |
| 3 | Sandhika Surya Ardyanto | 5025211022 |

### Daftar Isi
- [Soal 1](#soal-1)
    - [a. Download dan Unzip](#perintah-1-a)
    - [b. Analisis awal pada data](#perintah-1-b)
    - [c. Buat Dockerfile](#perintah-1-c)
    - [d. Mem-publish Docker Image sistem ke Docker Hub](#perintah-1-d)
    - [e. Buat Docker Compose dengan instance sebanyak 5](#perintah-1-e)
- [Soal 2](#soal-2)
    - [a. Membuat folder](#perintah-2-a)
    - [b. Mengubah nama folder](#perintah-2-b)
    - [c. Ubah folder filePenting di dalam projects menjadi secure](#perintah-2-c)
    - [d. Hapus semua fileLama](#perintah-2-d)
    - [e. Format logging](#perintah-2-e)
- [Soal 3](#soal-3)
    - [a. Membuat sebuah FUSE](#perintah-3-a)
    - [b. Folder dan file ter-encode Base64](#perintah-3-b)
    - [c. Membedakan antara file dan direktori](#perintah-3-c)
    - [d. Set password](#perintah-3-d)
    - [e. Mounting FUSE](#perintah-3-e)
    - [f. Eksekusi image](#perintah-3-f)
    
## Soal 1
Kota Manchester sedang dilanda berita bahwa kota ini mau juara (yang kota ‘loh ya, bukan yang satunya). Tagline #YBBA (#YangBiruBiruAja #anjaykelaspepnihbossenggoldong 💪🤖🤙🔵⚪) sudah mewabah di seluruh dunia. Semua warga pun sudah menyiapkan pesta besar-besaran. 
Seorang pelatih sepak bola handal bernama Peb merupakan pelatih klub Manchester Blue, sedang berjuang memenangkan Treble Winner. Untuk meraihnya, ia perlu melakukan pembelian pemain dengan ideal. Agar sukses, ia memahami setiap detail data performa pemain sepak bola seluruh dunia yang meliputi statistik pemain, umur, tinggi dan berat badan, potensi, klub dan negaranya, serta banyak data lainnya. Namun, tantangan tersendiri muncul ketika mengelola dan mengakses data berukuran besar ini.
Kesulitan Peb tersebut mencapai telinga kalian, seorang mahasiswa Teknik Informatika yang ahli dalam pengolahan data. Mengetahui tantangan Peb, kalian diminta untuk membantu menyelesaikan masalahnya melalui beberapa langkah berikut.

- ### Perintah 1-a
Langkah pertama, adalah memperoleh data yang akan digunakan. Kalian membuat file bernama storage.c. Oleh karena itu, download dataset tentang pemain sepak bola dari Kaggle. Dataset ini berisi informasi tentang pemain sepak bola di seluruh dunia, termasuk Manchester Blue. Kalian tahu bahwa dataset ini akan sangat berguna bagi Peb. Gunakan command ini untuk men-download dataset.

`kaggle datasets download -d bryanb/fifa-player-stats-database`

Setelah berhasil men-download dalam format .zip, langkah selanjutnya adalah mengekstrak file tersebut.  Kalian melakukannya di dalam file storage.c untuk semua pengerjaannya. 

```
    system("kaggle datasets download bryanb/fifa-player-stats-database");
    system("unzip fifa-player-stats-database.zip");
```
Keterangan :
- `system` digunakan untuk menjalankan perintah di dalam tanda kutip. 

Perintah pertama menjalankan `kaggle datasets download bryanb/fifa-player-stats-database` yang berarti mengunduh dataset bernama "fifa-player-stats-database" dari Kaggle. 

Perintah diatas menggabungkan perintah `kaggle datasets download` dengan argumen `bryanb/fifa-player-stats-database`, yang menunjukkan dataset yang ingin diunduh.

Lalu, pada baris selanjutnya menggunakan perintah system untuk mengekstrak file zip yang telah diunduh sebelumnya. Perintah yang dijalankan adalah `unzip fifa-player-stats-database.zip`, yang berarti mengekstrak file dengan nama "fifa-player-stats-database.zip".

- ### Perintah 1-b
Selanjutnya, Peb meminta kalian untuk melakukan analisis awal pada data tersebut dan mencari pemain berpotensi tinggi untuk direkrut. Oleh karena itu, kalian perlu membaca file CSV khusus bernama **FIFA23_official_data.csv** dan mencetak data pemain yang berusia di bawah 25 tahun, memiliki potensi di atas 85, dan bermain di klub lain **selain Manchester City**. Informasi yang dicetak mencakup nama pemain, klub tempat mereka bermain, umur, potensi, URL foto mereka, dan data lainnya. kalian melakukannya di dalam file **storage.c** untuk analisis ini.

```
FILE* file = fopen("FIFA23_official_data.csv", "r");
if (file == NULL) {
    printf("Gagal membuka file CSV.\n");
    return 1;
}
```
Keterangan:

Kode diatas menggunakan fungsi `fopen` untuk membuka file.

`FILE* file = fopen("FIFA23_official_data.csv", "r");` baris ini, dideklarasikan sebuah pointer file yang bertipe FILE*. Fungsi fopen digunakan untuk membuka file dengan nama "FIFA23_official_data.csv" dalam mode "r" yang menunjukkan bahwa kita hanya ingin membaca file tersebut. 

Hasil dari fungsi fopen akan diassign ke pointer file, sehingga pointer file akan menunjuk ke file yang berhasil dibuka.

`if (file == NULL) { ... }` dilakukan pengecekan apakah file berhasil dibuka atau tidak. Jika pointer file memiliki nilai NULL, itu berarti file gagal dibuka.

`printf("Gagal membuka file CSV.\n");` pesan "Gagal membuka file CSV." akan dicetak ke layar menggunakan fungsi `printf`. Pesan tersebut menunjukkan apabila ada kesalahan dalam membuka file CSV.

```
char line[MAX_LINE_LENGTH];
fgets(line, MAX_LINE_LENGTH, file);
```
Keterangan:

`char line[MAX_LINE_LENGTH];` mendeklarasikan sebuah array karakter line dengan ukuran maksimum MAX_LINE_LENGTH. Array ini akan digunakan untuk menyimpan baris teks yang dibaca dari file.

`fgets(line, MAX_LINE_LENGTH, file);` menggunakan fungsi `fgets` untuk membaca baris teks dari file yang ditunjuk oleh pointer file. 

Argumen pertama line menunjukkan di mana baris teks yang dibaca akan disimpan, argumen kedua MAX_LINE_LENGTH menunjukkan ukuran maksimum baris yang dapat dibaca, dan argumen ketiga file adalah pointer file yang telah dibuka sebelumnya. 

Fungsi `fgets` membaca baris teks dari file hingga mencapai ukuran maksimum atau menemui karakter newline ('\n') atau akhir file, dan menggantikan karakter newline dengan karakter null ('\0') untuk menandai akhir string.

```
printf("Nama Pemain | Klub | Umur | Potensi | PhotoURL\n");
```
Kode diatas digunakan untuk menampilkan Nama Pemain, Klub, Umur, dll untuk menjadi keterangan pada ouput dibawahnya.

```
 while (fgets(line, MAX_LINE_LENGTH, file)) {
        char* token;
        Player player;

        token = strtok(line, ",");
        strcpy(player.id, token);

        token = strtok(NULL, ",");
        strcpy(player.name, token);

        token = strtok(NULL, ",");
        player.age = atoi(token);

        token = strtok(NULL, ",");
        strcpy(player.photoUrl, token);

        token = strtok(NULL, ",");
        strcpy(player.nationality, token);

        token = strtok(NULL, ",");
        strcpy(player.flag, token);

        token = strtok(NULL, ",");
        player.overall = atoi(token);

        token = strtok(NULL, ",");
        player.potential = atoi(token);

        token = strtok(NULL, ",");
        strcpy(player.club, token);

        if (player.age < 25 && player.potential > 85 && strcmp(player.club, "Manchester City") != 0) {
            printf("%s | %s | %d | %d | %s\n", player.name, player.club, player.age, player.potential, player.photoUrl);
        }
    }
```
Kode tersebut membaca baris-baris teks dari file yang telah dibuka sebelumnya menggunakan `fgets`.

Setiap baris kemudian dipisahkan menjadi token-token menggunakan fungsi `strtok` dan nilai-nilai tersebut disimpan ke dalam struct Player. 

Selanjutnya, dilakukan beberapa pengecekan kondisi untuk setiap pemain yang terbaca, dan jika pemain memenuhi kriteria tertentu, informasi pemain tersebut akan dicetak ke layar menggunakan `printf`.

- Keterangan:

`while (fgets(line, MAX_LINE_LENGTH, file)) { ... }` menggunakan perulangan while untuk membaca baris-baris teks dari file yang telah dibuka sebelumnya. Setiap baris akan dibaca menggunakan fungsi fgets dan disimpan dalam array karakter line. Perulangan akan berlanjut selama fungsi fgets berhasil membaca baris teks.

`char* token;` digunakan untuk mendeklarasikan pointer karakter token. Pointer ini akan digunakan untuk menyimpan token-token yang dihasilkan oleh fungsi strtok.

`Player player;` digunakan untuk mendeklarasikan sebuah variabel player dengan tipe data Player.

`token = strtok(line, ",");` digunakan fungsi strtok untuk membagi baris teks menjadi token-token berdasarkan pemisah koma (','). 

Fungsi `strtok` akan mengembalikan pointer ke token pertama yang ditemukan. Pada pemanggilan pertama ini, digunakan line sebagai argumen pertama untuk menunjukkan bahwa kita ingin membagi line menjadi token-token. 

Argumen kedua "," menunjukkan bahwa pemisah yang digunakan adalah koma. Pointer token tersebut disimpan dalam variabel token.

`strcpy(player.id, token);` menggunakan fungsi `strcpy` untuk menyalin nilai token ke dalam anggota id dari struct player.

Langkah langkah diatas dilakukan pada looping untuk mendapatkan nilai token yang berbeda dan anggota yang sesuai dalam struct Player. Masing-masing anggota diisi dengan nilai dari token yang telah dipisahkan menggunakan fungsi strtok.

`if (player.age < 25 && player.potential > 85 && strcmp(player.club, "Manchester City") != 0) { ... }`  dilakukan pengecekan kondisi terhadap pemain yang telah dibaca. Jika pemain memenuhi kriteria yaitu umur di bawah 25, potensi di atas 85, dan bukan bermain untuk klub "Manchester City", maka blok kode di dalam kurung kurawal { ... } akan dieksekusi.

`printf("%s | %s | %d | %d | %s\n", player.name, player.club, player.age, player.potential, player.photoUrl);` informasi pemain yang memenuhi kriteria akan dicetak menggunakan printf. Informasi yang dicetak meliputi nama pemain, klub, umur, potensi, dan URL foto pemain.

- ### Perintah 1-c
Peb menyadari bahwa sistem kalian sangat berguna dan ingin sistem ini bisa diakses oleh asisten pelatih lainnya. Oleh karena itu, kalian perlu menjadikan sistem yang dibuat ke sebuah Docker Container agar mudah di-distribute dan dijalankan di lingkungan lain tanpa perlu setup environment dari awal. Buatlah Dockerfile yang berisi semua langkah yang diperlukan untuk setup environment dan menentukan bagaimana aplikasi harus dijalankan.
Setelah Dockerfile berhasil dibuat, langkah selanjutnya adalah membuat Docker Image. Gunakan Docker CLI untuk mem-build image dari Dockerfile kalian. Setelah berhasil membuat image, verifikasi bahwa image tersebut berfungsi seperti yang diharapkan dengan menjalankan Docker Container dan memeriksa output-nya.

```
FROM ubuntu:latest

RUN apt-get update && apt-get install -y python3 python3-pip gcc unzip

RUN pip3 install kaggle

RUN mkdir -p /root/.kaggle && \
    echo '{"username":"gabriellanatasya","key":"6c8ff722ec2aaa7fea562c8abfbc476c"}' > /root/.kaggle/kaggle.json && \
    chmod 600 /root/.kaggle/kaggle.json

ENV PATH=$PATH:/root/.local/bin

WORKDIR /storage-app

COPY Dockerfile /storage-app/Dockerfile

COPY storage.c /storage-app/storage.c

RUN gcc -o storage storage.c

CMD ["./storage"]
```
Keterangan:

`FROM ubuntu:latest` mendefinisikan base image yang akan digunakan untuk membangun image Docker. Dalam hal ini, menggunakan image "ubuntu:latest" sebagai base image. Artinya, akan membangun image Docker di atas image Ubuntu terbaru.

`RUN apt-get update && apt-get install -y python3 python3-pip gcc unzip` menggunakan perintah `RUN` untuk menjalankan perintah-perintah dalam lingkungan Docker. Di sini melakukan pembaruan (apt-get update) dan menginstal beberapa paket yang diperlukan seperti Python 3, pip untuk Python 3, gcc (GNU Compiler Collection), dan unzip.

`RUN pip3 install kaggle` menggunakan perintah `RUN` untuk menginstal paket kaggle menggunakan pip3, untuk mengakses dan mengunduh dataset dari platform Kaggle.

```
RUN mkdir -p /root/.kaggle && \
echo '{"username":"gabriellanatasya","key":"6c8ff722ec2aaa7fea562c8abfbc476c"}' > /root/.kaggle/kaggle.json && \
chmod 600 /root/.kaggle/kaggle.json
```
Menggunakan perintah `RUN` untuk membuat direktori `.kaggle` di dalam direktori /root dan membuat file `kaggle.json` di dalamnya. 

File `kaggle.json` berisi informasi otentikasi yang diperlukan untuk mengakses API Kaggle. Selanjutnya,mengatur izin akses file `kaggle.json` menjadi 600 agar hanya dapat dibaca dan ditulis oleh pemilik.

`ENV PATH=$PATH:/root/.local/bin` menggunakan perintah ENV untuk mengatur variabel lingkungan PATH di dalam container Docker. Ditambahkan `/root/.local/bin` ke variabel PATH, sehingga dapat menjalankan perintah-perintah yang ada di dalam direktori tersebut.

`WORKDIR /storage-app` menggunakan perintah `WORKDIR` untuk mengatur direktori kerja di dalam container menjadi `/storage-app`. Ini akan menjadi direktori aktif default saat menjalankan perintah-perintah berikutnya.

`COPY Dockerfile /storage-app/Dockerfile` menggunakan perintah `COPY` untuk menyalin file `Dockerfile` dari direktori yang sama dengan Dockerfile ke direktori `/storage-app` di dalam container.

`COPY storage.c /storage-app/storage.c` menggunakan perintah `COPY` untuk menyalin file storage.c dari direktori yang sama dengan Dockerfile ke direktori /storage-app di dalam container.

`RUN gcc -o storage storage.c` menggunakan perintah `RUN` untuk menjalankan perintah gcc di dalam container. Ini akan mengkompilasi file storage.c dan menghasilkan file storage, yang akan digunakan dalam langkah selanjutnya.

`CMD ["./storage"]` perintah `CMD` untuk menentukan perintah yang akan dijalankan secara default saat container dijalankan. Di sini, perintah yang ditentukan adalah ./storage, yang menjalankan file storage yang telah dikompilasi sebelumnya.

- ### Perintah 1-d
Setelah sukses membuat sistem berbasis Docker, Peb merasa bahwa sistem ini tidak hanya berguna untuk dirinya sendiri, tetapi juga akan akan membantu para scouting-nya yang terpencar di seluruh dunia dalam merekrut pemain berpotensi tinggi. Namun, satu tantangan muncul, yaitu bagaimana caranya para scout dapat mengakses dan menggunakan sistem yang telah diciptakan?
Merasa terpanggil untuk membantu Peb lebih jauh, kalian memutuskan untuk mem-publish Docker Image sistem ke Docker Hub, sebuah layanan cloud yang memungkinkan kalian untuk membagikan aplikasi Docker kalian ke seluruh dunia. Output dari pekerjaan ini adalah file Docker kalian bisa dilihat secara public pada https://hub.docker.com/r/{Username}/storage-app.

```
https://hub.docker.com/r/gabriellan/storage-app
```
Link diatas adalah link yang sudah di push ke Docker Hub.

- ### Perintah 1-e
Berita tagline #YBBA (#YangBiruBiruAja) semakin populer, dan begitu juga sistem yang telah kalian buat untuk membantu Peb dalam rekrutmen pemain. Beberapa klub sepak bola lain mulai menunjukkan minat pada sistem tersebut dan permintaan penggunaan semakin bertambah. Untuk memastikan sistem kalian mampu menangani lonjakan permintaan ini, kalian memutuskan untuk menerapkan skala pada layanan menggunakan Docker Compose dengan instance sebanyak 5. Buat folder terpisah bernama Barcelona dan Napoli dan jalankan Docker Compose di sana.

```
version: "3"
services:
  storage-app:
    image: storage-app
    ports:
      - "8080-8089:8080"
    deploy:
      replicas: 5

```
Keterangan:

`version: "3"` mendefinisikan versi dari format konfigurasi docker-compose yang digunakan. Dalam hal ini, menggunakan versi "3".

`services:` mulai mendefinisikan layanan yang akan dijalankan dalam lingkungan Docker.

`storage-app:` memberi nama untuk layanan yang akan dijalankan. Dalam hal ini, layanan tersebut dinamakan "storage-app".

`image: storage-app` menentukan image yang akan digunakan untuk layanan "storage-app". Image ini akan dibangun sebelumnya menggunakan Dockerfile yang sudah definisikan.

`ports:` mendefinisikan pengaturan port untuk layanan "storage-app". Port yang ditentukan adalah "8080-8089:8080", yang berarti port di host akan diarahkan ke port 8080 dalam kontainer "storage-app". Dengan konfigurasi ini, dan dapat mengakses layanan yang berjalan di port 8080 pada host.

`deploy:` mendefinisikan pengaturan penyebaran (deployment) untuk layanan "storage-app". Penyebaran ini mengatur bagaimana layanan akan didistribusikan ke beberapa instance kontainer.

`replicas: 5` menentukan jumlah replika atau instance yang akan dijalankan untuk layanan "storage-app". Dalam hal ini, terdapat 5 replika yang akan dijalankan.

## Soal 2
Nana adalah peri kucing yang terampil dalam menggunakan sihir, dia bisa membuat orang lain berubah menjadi Molina. Suatu hari, dia bosan menggunakan sihir dan mencoba untuk magang di germa.dev sebagai programmer.
Agar dapat diterima sebagai karyawan magang dia diberi sebuah file .zip yang berisi folder dan file dari Germa. Kemudian, Nana harus membuat sistem manajemen folder dengan ketentuan sebagai berikut:
- Apabila terdapat file yang mengandung kata restricted, maka file tersebut tidak dapat di-rename ataupun dihapus.
- Apabila terdapat folder yang mengandung kata restricted, maka folder tersebut, folder yang ada di dalamnya, dan file yang ada di dalamnya tidak dapat di-rename ataupun dihapus.
Contoh:
    /jalan/keputih/perintis/iv/sangatrestrictedloh/gang/kane.txt

Karena latar belakang Nana adalah seorang penyihir, dia ingin membuat satu kata sihir yaitu bypass yang dapat menghilangkan kedua aturan tadi. Kata sihir tersebut akan berlaku untuk folder dan file yang ada di dalamnya.

Case invalid untuk bypass:

`/jalan/keputih/perintis/iv/tidakrestrictedlohini/gang/kane.txt
    /jalan/keputih/perintis/iv/sangatrestrictedloh/gangIV/kane.txt
    /jalan/keputih/perintis/iv/sangatrestrictedloh/gang/kanelagiramai.txt`

Case valid untuk bypass:

`/jalan/keputih/perintis/iv/tadirestrictedtapibypasskok/gang/kane.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/bypassbanget/kane.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/bypassbanget/kaneudahtutup.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/gang/kanepakaibypass.txt`

Setelah Nana paham aturan tersebut, bantu Nana untuk membuat sebuah FUSE yang bernama germa.c, yang mana dapat melakukan make folder, rename file dan folder, serta delete file dan folder. Untuk mengujinya, maka lakukan hal-hal berikut:

- ### Perintah 2-a
Buatlah folder productMagang pada folder /src_data/germa/products/restricted_list/. Kemudian, buatlah folder projectMagang pada /src_data/germa/projects/restricted_list/. Akan tetapi, hal tersebut akan gagal.

```
int is_restricted(const char *path){
    return strstr(path, "restricted") != NULL;
}

int mkdir(const char *path, mode_t mode) {
    char fpath[10069];
    sprintf(fpath, "%s%s", temp_dir, path);

    int res = mkdir(fpath, mode);
    if (res == -1) return -errno;

    return 0;
}

```

is_restricted, sesuai namanya, digunakan untuk mengecek apakah pada path ada kata restricted di dalamnya. mkdir sendiri digunakan untuk make directory

- ### Perintah 2-b
Ubahlah nama folder dari restricted_list pada folder /src_data/germa/projects/restricted_list/ menjadi /src_data/germa/projects/bypass_list/. Kemudian, buat folder projectMagang di dalamnya.

```
int rename(const char *asal, const char *tujuan) {
    char fpath_a[10069];
    char fpath_t[10069];
    sprintf(fpath_a, "%s%s", temp_dir, asal);
    sprintf(fpath_t, "%s%s", temp_dir, tujuan);

    if (is_restricted(fpath_a) || is_restricted(fpath_t)) return -EPERM;

    int res = rename(fpath_a, fpath_t);
    if (res == -1) return -errno;

    return 0;
}
```

Bagian ini digunakan untuk melakukan rename pada path

- ### Perintah 2-c
Karena folder projects menjadi dapat diakses, ubahlah folder filePenting di dalam folder projects menjadi restrictedFilePenting agar secure kembali. Coba keamanannya dengan mengubah nama file yang ada di dalamnya.

```

```

- ### Perintah 2-d
Terakhir, coba kamu hapus semua fileLama di dalam folder restrictedFileLama. Jangan lupa untuk menambahkan kata sihir pada folder tersebut agar folder tersebut dapat terhapus

```

```

- ### Perintah 2-e
Semua kegiatan yang kalian lakukan harus tercatat dalam sebuah file logmucatatsini.txt di luar file .zip yang diberikan kepada Nana, agar pekerjaan yang kamu lakukan dapat dilaporkan kepada Germa. 
Adapun format logging yang harus kamu buat adalah sebagai berikut.

`[STATUS]::[dd]/[MM]/[yyyy]-[HH]:[mm]:[ss]::[CMD]::[DESC]`

Dengan keterangan sebagai berikut.

- STATUS: SUCCESS atau FAILED.
- dd: 2 digit tanggal.
- MM: 2 digit bulan.
- yyyy: 4 digit tahun.
- HH: 24-hour clock hour, with a leading 0 (e.g. 22).
- mm: 2 digit menit.
- ss: 2 digit detik.
- -CMD: command yang digunakan (MKDIR atau RENAME atau RMDIR atau RMFILE atau lainnya).
- DESC: keterangan action, yaitu:

    [User]-Create directory x.

    [User]-Rename from x to y.

    [User]-Remove directory x.

    [User]-Remove file x.

Contoh:

`SUCCESS::17/05/2023-19:31:56::RENAME::Oky-Rename from x to y`

```

```

## Soal 3
Dhafin adalah seorang yang sangat pemalu, tetapi saat ini dia sedang mengagumi seseorang yang dia tidak ingin seorangpun tahu. Maka dari itu, dia melakukan berbagai hal sebagai berikut.

### Perintah 3-a
Dhafin ingin membuat sebuah FUSE yang termodifikasi dengan source mount-nya adalah /etc yang bernama secretadmirer.c. 

```
int exec(char path[], char *argv[]){
    pid_t child_id;
    int status;

    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE);
    }

    if (child_id == 0) {
        execvp(path, argv);
    } 
    else{
        while ((wait(&status)) > 0);
    }
}
```
- Fungsi exec untuk menjalankan fungsi command linux yang digunakan untuk memproses link
- Menggunakan execvp untuk mengeksekusi child process lalu menunggu selesainya eksekusi dengan wait

```
void ext_file(char link[]){
    char *argv[] = {"wget", "--no-check-certificate", "-O", "inifolderetc.zip", link, NULL};
    exec(argv[0], argv);
    char *arg[] = {"unzip", "-q", "inifolderetc.zip", NULL};
    exec(arg[0], arg);
}
```
- Fungsi ext_file berisi command linux yang digunakan untuk memproses link
- wget untuk mengunduh file dari link 
- unzip untuk mengekstrak file zip hasil dari wget


### Perintah 3-b
Lalu, untuk menutupi rahasianya, Dhafin ingin semua direktori dan file yang berada pada direktori yang diawali dengan huruf L, U, T, dan H akan ter-encode dengan Base64.  Encode berlaku rekursif (berlaku untuk direktori dan file yang baru di-rename atau pun yang baru dibuat).

```
DIR *dir = opendir(directory);
    if (dir == NULL) {
        printf("Failed to open directory '%s'\n", directory);
        return;
    }

    struct dirent *dp;
    while ((dp = readdir(dir)) != NULL) {
        if (dp->d_type == DT_REG || dp->d_type == DT_DIR) {
            if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0) {
                if (dp->d_type == DT_DIR) {                    
                    char subdir[BUFFER_SIZE];
                    snprintf(subdir, BUFFER_SIZE, "%s/%s", directory, dp->d_name);
                    DIR *ep = opendir(subdir);
                    if (ep == NULL) {
                        printf("Failed to open subdirectory '%s'\n", subdir);
                        continue;
                    }
                    dirOp(subdir);
                    closedir(ep);
                }
```
- Menggunakan opendir dan closedir dari library dirent.h untuk mengakses file dan direktori dari direktori "inifolderetc"
- Fungsi bersifat rekursif dimana setiap menemukan subdirektori maka akan menjalankan dirOp(subdir) untuk masuk ke subdirektori tersebut
```
if(ch == 'l' || ch == 'u' || ch == 't' || ch == 'h') {
    if(dp->d_type == DT_REG) {
        FILE *file = fopen(path, "rb");
        if (file != NULL) {
            fseek(file, 0, SEEK_END);
            long file_size = ftell(file);
            fseek(file, 0, SEEK_SET);

            unsigned char *file_data = (unsigned char *)malloc(file_size);
            if (file_data != NULL) {
                fread(file_data, 1, file_size, file);
                BIO *b64_file = BIO_new(BIO_f_base64());
                BIO *bio_file = BIO_new(BIO_s_mem());
                BIO_push(b64_file, bio_file);

                BIO_write(b64_file, file_data, file_size);
                BIO_flush(b64_file);

                BUF_MEM *buffer_file;
                BIO_get_mem_ptr(b64_file, &buffer_file);

                FILE *encoded_file = fopen(path, "wb");
                if (encoded_file != NULL) {
                    fwrite(buffer_file->data, 1, buffer_file->length, encoded_file);
                    fclose(encoded_file);
                    printf("Encoded content of '%s'\n", path);
                } 
                else {
                    printf("Failed to encode file '%s'\n", path);
                }
                free(file_data);
                BIO_free_all(b64_file);
            } 
            else {
                printf("Failed to allocate memory for file data\n");
            }
            fclose(file);
        } 
        else {
            printf("Failed to open file '%s'\n", path);
        }                         
    }
}
```
- Jika merupakan file dan bukan direktori yang diawali 'l', 'u', 't', atau 'h' maka akan melakukan encode base64 terhadap isi file
- Menggunakan *file untuk membaca isi file yang diencode dengan BIO dan hasilnya ditulis kembali dengan fwrite dari *encoded_file  

### Perintah 3-c
Untuk membedakan antara file dan direktori, Dhafin ingin semua format penamaan file menggunakan lowercase sedangkan untuk direktori semua harus uppercase. Untuk nama file atau direktori yang hurufnya kurang dari atau sama dengan empat, maka ubah namanya menggunakan data format binary yang didapat dari ASCII code masing-masing karakter.

``` 
void toBinary(char txt[]){    
    int length = strlen(txt);
    for(int i=0; i<length; i++){
        int asciiCode = txt[i];
        char temp[] = "11111111";
        for(int j=7; j>=0; j--){
            temp[j] = asciiCode%2 == 0 ? '0' : '1';
            asciiCode /= 2;
        }
        if(i == 0){
            sprintf(biner, "%s", temp);
        }
        else{
            sprintf(biner, "%s %s", biner, temp);
        }
    }
}
```
- Fungsi toBinary untuk mengubah karakter dari nama file atau direktori dimana jumlahnya kurang dari 4 menjadi representasi biner dari tabel ASCII
- Mengubah karakter menjadi urutan pada tabel ASCII kemudian menjadikan bilangan biner berdasarkan apa hasilnya habis dibagi 2
- sprintf untuk membuat hasil akhir representasi biner dari kumpulan karakter
```
int fileLen(const char* filename) {
    int length = strlen(filename);
    int extLen = 0;

    const char* separator = strrchr(filename, '.');
    if (separator != NULL) {
        extLen = length - (dot - filename) - 1;
    }
    length -= extLen;

    return length;
}
```
- Fungsi fileLen untuk menemukan panjang nama file yang dipisahkan oleh '.' sebagai extension
```
char path[BUFFER_SIZE];
snprintf(path, BUFFER_SIZE, "%s/%s", directory, dp->d_name);

char name[BUFFER_SIZE];
snprintf(name, BUFFER_SIZE, "%s", dp->d_name);

size_t fl_name = fileLen(name);

snprintf(path, BUFFER_SIZE, "%s/%s", directory, name);

if(fl_name < 4){
    toBinary(name);   
    char new_path[BUFFER_SIZE];
    snprintf(path, BUFFER_SIZE, "%s/%s", directory, name);
    snprintf(new_path, BUFFER_SIZE, "%s/%s", directory, biner);
    if (rename(path, new_path) == 0) {
        printf("File renamed successfully to: %s\n", biner);
        snprintf(path, BUFFER_SIZE, "%s", new_path);
    } 
    else {
        printf("Failed to rename the file.\n");
    }                 
}
else{
    if (dp->d_type == DT_REG) {
        for (int i = 0; name[i] != '\0'; i++) {
            name[i] = tolower(name[i]);
        }
    } 
    else if (entry->d_type == DT_DIR) {
        for (int i = 0; entry->d_name[i] != '\0'; i++) {
            name[i] = toupper(name[i]);
        }
    }
    char new_path[BUFFER_SIZE];
    nprintf(new_path, BUFFER_SIZE, "%s/%s", directory, name);
    if (rename(path, new_path) == 0) {
        printf("Renamed '%s' to '%s'\n", path, new_path);
        snprintf(path, BUFFER_SIZE, "%s", new_path);
    } 
    else {
        printf("Failed to rename '%s'\n", path);
    }
}
```
- Jika karakter kurang dari 4 maka akan diubah menjadi bilangan biner dan digunakan untuk rename file atau direktori
- Jika karakter lebih dari atau sama dengan 4 maka akan diubah menjadi huruf kecil dengan tolower untuk file (DT_REG) dan menjadi huruf besar dengan toupper untuk direktori (DT_DIR)


### Perintah 3-d
Setiap ingin membuka file, maka harus memasukkan password terlebih dahulu (set password tidak ditentukan a.k.a terserah).

```
static int xmp_open(const char *path, struct fuse_file_info *fi){
    char password[20];
    printf("Masukkan password: ");
    scanf("%s", password);

    if (strcmp(password, "sisopetc") != 0) {
        printf("Password salah\n");
        return -EACCES;
    }
    int fd = open(path, fi->flags);
    if (fd == -1)
        return -errno;

    fi->fh = fd;
    return 0;
}
```
- Setiap kali berusaha membuka file akan memasukkan password dan akan dicek kesesuainnya dengan strcmp
- Jika salah memasukkan password maka akan return -EACCES yang memberikan kode error EACCES


### Perintah 3-e
Selanjutnya, Dhafin ingin melakukan mounting FUSE tersebut di dalam Docker Container menggunakan Docker Compose (gunakan base image Ubuntu Bionic ). Ketentuannya, terdapat dua buah container sebagai berikut.
- Container bernama Dhafin akan melakukan mount FUSE yang telah dimodifikasi tersebut. 
- Container bernama Normal akan melakukan mount yang hanya menampilkan /etc yang normal tanpa ada modifikasi apapun. 

---
```
version: '3'
services:
  Dhafin:
    image: ubuntu:bionic
    volumes:
      - ./inifolderetc:/inifolderetc
      - secretadmirer:secretadmirer

  Normal:
    image: ubuntu:bionic
    volumes:
      - ./inifolderetc:/inifolderetc
```
- version merupakan syntax file Docker Compose yang digunakan 
- Menggunakan image "Ubuntu Bionic"
- Container Dhafin berisi direktori "inifolderetc" dan output program "secretadmirer.c" untuk modifikasi direktori
- Container Normal hanya berisi "inifolderetc" yang belum dimodifikasi

### Perintah 3-f
Kalian masih penasaran siapa yang dikagumi Dhafin? 🥰😍😘😚🤗😻💋💌💘💝💖💗 Coba eksekusi image fhinnn/sisop23 deh, siapa tahu membantu. 🙂

```
docker build fhinnn/sisop23:latest
```
