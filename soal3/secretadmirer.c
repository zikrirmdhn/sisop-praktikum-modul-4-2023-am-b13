#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <stdlib.h>
#include <wait.h>
#include <sys/types.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <openssl/buffer.h>
#include <ctype.h>
#define BUFFER_SIZE 1024


int exec(char path[], char *argv[]){
    pid_t child_id;
    int status;

    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE);
    }

    if (child_id == 0) {
        execvp(path, argv);
    } 
    else{
        while ((wait(&status)) > 0);
    }
}

void ext_file(char link[]){
    char *argv[] = {"wget", "--no-check-certificate", "-O", "inifolderetc.zip", link, NULL};
    exec(argv[0], argv);
    char *arg[] = {"unzip", "-q", "inifolderetc.zip", NULL};
    exec(arg[0], arg);
}

char biner[50];

void toBinary(char txt[]){    
    int length = strlen(txt);
    for(int i=0; i<length; i++){
        int asciiCode = txt[i];
        char temp[] = "11111111";
        for(int j=7; j>=0; j--){
            temp[j] = asciiCode%2 == 0 ? '0' : '1';
            asciiCode /= 2;
        }
        if(i == 0){
            sprintf(biner, "%s", temp);
        }
        else{
            sprintf(biner, "%s %s", biner, temp);
        }
    }
}

int fileLen(const char* filename) {
    int length = strlen(filename);
    int extLen = 0;

    const char* separator = strrchr(filename, '.');
    if (separator != NULL) {
        extLen = length - (dot - filename) - 1;
    }
    length -= extLen;

    return length;
}


void dirOp(const char* directory) {
    DIR *dir = opendir(directory);
    if (dir == NULL) {
        printf("Failed to open directory '%s'\n", directory);
        return;
    }

    struct dirent *dp;
    while ((dp = readdir(dir)) != NULL) {
        if (dp->d_type == DT_REG || dp->d_type == DT_DIR) {
            if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0) {
                if (dp->d_type == DT_DIR) {                    
                    char subdir[BUFFER_SIZE];
                    snprintf(subdir, BUFFER_SIZE, "%s/%s", directory, dp->d_name);
                    DIR *ep = opendir(subdir);
                    if (ep == NULL) {
                        printf("Failed to open subdirectory '%s'\n", subdir);
                        continue;
                    }
                    dirOp(subdir);
                    closedir(ep);
                }

                char ch = tolower(dp->d_name[0]);

                char path[BUFFER_SIZE];
                snprintf(path, BUFFER_SIZE, "%s/%s", directory, dp->d_name);

                char name[BUFFER_SIZE];
                snprintf(name, BUFFER_SIZE, "%s", dp->d_name);

                size_t fl_name = fileLen(name);

                snprintf(path, BUFFER_SIZE, "%s/%s", directory, name);

                if(fl_name < 4){
                    toBinary(name);   
                    char new_path[BUFFER_SIZE];
                    snprintf(path, BUFFER_SIZE, "%s/%s", directory, name);
                    snprintf(new_path, BUFFER_SIZE, "%s/%s", directory, biner);
                    if (rename(path, new_path) == 0) {
                        printf("File renamed successfully to: %s\n", biner);
                        snprintf(path, BUFFER_SIZE, "%s", new_path);
                    } 
                    else {
                        printf("Failed to rename the file.\n");
                    }                 
                }
                else{
                    if (dp->d_type == DT_REG) {
                        for (int i = 0; name[i] != '\0'; i++) {
                            name[i] = tolower(name[i]);
                        }
                    } 
                    else if (entry->d_type == DT_DIR) {
                        for (int i = 0; entry->d_name[i] != '\0'; i++) {
                            name[i] = toupper(name[i]);
                        }
                    }
                    char new_path[BUFFER_SIZE];
                    snprintf(new_path, BUFFER_SIZE, "%s/%s", directory, name);
                    if (rename(path, new_path) == 0) {
                        printf("Renamed '%s' to '%s'\n", path, new_path);
                        snprintf(path, BUFFER_SIZE, "%s", new_path);
                    } 
                    else {
                        printf("Failed to rename '%s'\n", path);
                    }
                }
                if(ch == 'l' || ch == 'u' || ch == 't' || ch == 'h') {
                    if(dp->d_type == DT_REG) {
                        FILE *file = fopen(path, "rb");
                        if (file != NULL) {
                            fseek(file, 0, SEEK_END);
                            long file_size = ftell(file);
                            fseek(file, 0, SEEK_SET);

                            unsigned char *file_data = (unsigned char *)malloc(file_size);
                            if (file_data != NULL) {
                                fread(file_data, 1, file_size, file);
                                BIO *b64_file = BIO_new(BIO_f_base64());
                                BIO *bio_file = BIO_new(BIO_s_mem());
                                BIO_push(b64_file, bio_file);

                                BIO_write(b64_file, file_data, file_size);
                                BIO_flush(b64_file);

                                BUF_MEM *buffer_file;
                                BIO_get_mem_ptr(b64_file, &buffer_file);

                                FILE *encoded_file = fopen(path, "wb");
                                if (encoded_file != NULL) {
                                    fwrite(buffer_file->data, 1, buffer_file->length, encoded_file);
                                    fclose(encoded_file);
                                    printf("Encoded content of '%s'\n", path);
                                } 
                                else {
                                    printf("Failed to encode file '%s'\n", path);
                                }
                                free(file_data);
                                BIO_free_all(b64_file);
                            } 
                            else {
                                printf("Failed to allocate memory for file data\n");
                            }
                            fclose(file);
                        } 
                        else {
                            printf("Failed to open file '%s'\n", path);
                        }                         
                    }
                }
            }
        }
    }
    closedir(dir);
}


static  int  xmp_getattr(const char *path, struct stat *stbuf){
    int res;
    char fpath[1000];

    sprintf(fpath,"%s%s", source_mount, path);

    res = lstat(fpath, stbuf);

    if (res == -1) return -errno;

    return 0;
}

static int xmp_readdir(const char* path, void* buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info* fi)
{
    DIR* dir = opendir(path);
    if (dir == NULL) {
        return -errno;
    }

    struct dirent* dp;
    while ((dp = readdir(dir)) != NULL) {
        if (filler(buf, dp->d_name, NULL, 0) != 0) {
            break;
        }
    }

    closedir(dir);
    return 0;
}

static int xmp_mkdir(const char *path, mode_t mode){
    int res = mkdir("example", mode);
    if (res == -1)
        return -errno;

    return 0;
}

static int xmp_rename(const char *from, const char *to){
    int res = rename(from, to);
    if (res == -1)
        return -errno;
    return 0;
}

static int xmp_open(const char *path, struct fuse_file_info *fi){
    char password[20];
    printf("Masukkan password: ");
    scanf("%s", password);

    if (strcmp(password, "sisopetc") != 0) {
        printf("Password salah\n");
        return -EACCES;
    }
    int fd = open(path, fi->flags);
    if (fd == -1)
        return -errno;

    fi->fh = fd;
    return 0;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .open = xmp_open;
    .mkdir = xmp_mkdir;
    .rename = xmp_rename;
};

int  main(int  argc, char *argv[]){
    ext_file("drive.google.com/u/3/uc?id=1xBFLFSOsN-DuQCAkKWxyk4GlFH8SprlT&export=download&confirm=yes");
    dirOp("inifolderetc");
    returnfuse_main(argc, argv, &xmp_oper, NULL);
}