#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

static const char *temp_dir = "/home/zikri/Documents";

int is_restricted(const char *path){
    return strstr(path, "restricted") != NULL;
}

int getattr(const char *path, struct stat *buffer) {
    char fpath[10069];
    sprintf(fpath, "%s%s", temp_dir, path);

    int res = lstat(fpath, buffer);
    if (res == -1) return -errno;

    return 0;
}

int mkdir(const char *path, mode_t mode) {
    char fpath[10069];
    sprintf(fpath, "%s%s", temp_dir, path);

    int res = mkdir(fpath, mode);
    if (res == -1) return -errno;

    return 0;
}

int rename(const char *asal, const char *tujuan) {
    char fpath_a[10069];
    char fpath_t[10069];
    sprintf(fpath_a, "%s%s", temp_dir, asal);
    sprintf(fpath_t, "%s%s", temp_dir, tujuan);

    if (is_restricted(fpath_a) || is_restricted(fpath_t)) return -EPERM;

    int res = rename(fpath_a, fpath_t);
    if (res == -1) return -errno;

    return 0;
}


static struct fuse_operations germa_operations = {
    .getattr    = getattr,
    .mkdir      = mkdir,
    .rename     = rename,
};

int main(int argc, char *argv[]) {
    umask(0);
    return fuse_main(argc, argv, &germa_operations, NULL);
}
