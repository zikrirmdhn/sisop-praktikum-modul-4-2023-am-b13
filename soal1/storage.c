#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LINE_LENGTH 1000
#define MAX_FIELD_LENGTH 100

typedef struct {
    char id[MAX_FIELD_LENGTH];
    char name[MAX_FIELD_LENGTH];
    int age;
    char photoUrl[MAX_FIELD_LENGTH];
    char nationality[MAX_FIELD_LENGTH];
    char flag[MAX_FIELD_LENGTH];
    int overall;
    int potential;
    char club[MAX_FIELD_LENGTH];
} Player;

int main() {
    system("kaggle datasets download bryanb/fifa-player-stats-database");
    system("unzip fifa-player-stats-database.zip");

    FILE* file = fopen("FIFA23_official_data.csv", "r");
    if (file == NULL) {
        printf("Gagal membuka file CSV.\n");
        return 1;
    }

    char line[MAX_LINE_LENGTH];
    fgets(line, MAX_LINE_LENGTH, file);

    printf("Nama Pemain | Klub | Umur | Potensi | PhotoURL\n");

    while (fgets(line, MAX_LINE_LENGTH, file)) {
        char* token;
        Player player;

        token = strtok(line, ",");
        strcpy(player.id, token);

        token = strtok(NULL, ",");
        strcpy(player.name, token);

        token = strtok(NULL, ",");
        player.age = atoi(token);

        token = strtok(NULL, ",");
        strcpy(player.photoUrl, token);

        token = strtok(NULL, ",");
        strcpy(player.nationality, token);

        token = strtok(NULL, ",");
        strcpy(player.flag, token);

        token = strtok(NULL, ",");
        player.overall = atoi(token);

        token = strtok(NULL, ",");
        player.potential = atoi(token);

        token = strtok(NULL, ",");
        strcpy(player.club, token);

        if (player.age < 25 && player.potential > 85 && strcmp(player.club, "Manchester City") != 0) {
            printf("%s | %s | %d | %d | %s\n", player.name, player.club, player.age, player.potential, player.photoUrl);
        }
    }

    fclose(file);

    return 0;
}
